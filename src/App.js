import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from '@material-ui/core/Button'

class App extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      active: false
    }

    this.clickAction = this.clickAction.bind(this);
  }

  clickAction() {
    console.log("Click action");
    this.setState({
      active: !this.state.active
    });
  }

  render() {
    const styles = {
      transition: {
        width: "300px",
        marginLeft: "0px",
        overflow: "hidden",
      },
      transitionActive: {
        width: "200px",
        marginLeft: "100px",
        transition: "all 300ms ease-in-out",
        overflow: "hidden",
      }
    }

    let currentStyle = styles.transition;
    if(this.state.active === true) {
      currentStyle = styles.transitionActive;
    }

    return (
      <div className="App">
        <Button onClick={this.clickAction}>Hello World! Click Me!</Button>
        <div className="Wrapper" >
          <div className="Background">
            <div style={currentStyle}>
              <div className="BackgroundB">
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default (App);
